# safe delete

introduction to linux assignment

this script is made for linux assignment iTi

the script is made as specified in the assignment : 
https://classroom.google.com/u/0/c/MzgxMDA1OTQwMjVa/a/Mzk3NDg5MzUyMDVa/details

+ creates a hidden TRASH directory in user's home directory
+ adds it self to the cron daemon list if the script isn't already there
+ can handle folder deletion
